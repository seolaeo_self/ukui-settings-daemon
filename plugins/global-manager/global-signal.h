#ifndef GSETTINGSCONFIG_H
#define GSETTINGSCONFIG_H

#include <QObject>
#include <QMetaType>
#include <QDBusReply>
#include <QDBusMessage>
#include <QDBusMetaType>
#include <QDBusInterface>
#include <QDBusPendingReply>

#include "clib-syslog.h"
#include "qdbus-login1-abstract-interface.h"

class GlobalSignal: public QObject
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface","org.ukui.SettingsDaemon.GlobalSignal")

public:
    GlobalSignal(QObject *parent = nullptr);

Q_SIGNALS:

   /**
    * @brief userActive
    * @return
    */
    int userActive();
public Q_SLOTS:

    /**
     * @brief sendUserActiveSignal
     */
    void sendUserActiveSignal(QString interface, QVariantMap qvarMap, QStringList strList);
private:
    /**
     * @brief connectUserActiveSignal
     */
    void connectUserActiveSignal();

    void connectUserActiveSignalWithPath(QString path);

    void connectUserLogin1Signal();
private:
    DBusLogin1Interface *m_plogin1;
    QDBusInterface *m_plogin2;
    QDBusInterface *m_ploginWithPath;
};

#endif // GSETTINGSCONFIG_H
