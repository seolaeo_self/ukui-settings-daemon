#ifndef BRIGHTNESS_H
#define BRIGHTNESS_H

#include <QObject>
#include <QDBusReply>
#include <QDBusInterface>

#include "clib-syslog.h"
#include "usd_base_class.h"
#include "abstract-brightness.h"
#include "gamma-brightness.h"
#include "powermanager-brightness.h"

class Brightness: public QObject
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", DBUS_GC_BRIGHTNESS_INTERFACE)
public:
    Brightness(QObject *parent = nullptr);

public Q_SLOTS:

    /**
     * @brief getPrimaryBrightness
     * @return
     */
    Q_SCRIPTABLE uint getPrimaryBrightness();

    /**
     * @brief setPrimaryBrightness
     * @return
     */
    Q_SCRIPTABLE bool setPrimaryBrightness(uint Brightness);

    /**
     * @brief isEnable
     * @return
     */
    Q_SCRIPTABLE uint isEnable();

    /**
     * @brief isEnable
     * @return
     */
    Q_SCRIPTABLE QString backend();

Q_SIGNALS:

    /**
     * @brief primaryChanged
     * @param Bright
     * @return
     */
    int primaryChanged(int Bright);

    /**
     * @brief enableChanged
     * @param Bright
     * @return
     */
    int enableChanged(bool state);

private:
    bool m_powerManagerSetable = false;
    bool m_gammaManagerSetable = false;

    AbstractBrightness *m_brightnessManager = nullptr;
};

#endif // BRIGHTNESS_H
