#include "brightness.h"
/*
 * 主屏机制问题：如果主屏没有在台式机外接的任意一台机器，主屏如何处理？
 * 1：USD无法感知，通过QApplication获取的主屏，与panel、peony实际位置并不相符合。。
*/
Brightness::Brightness(QObject *parent)
{
    QDBusInterface powerInterface("org.ukui.powermanagement",
                                  "/",
                                  "org.ukui.powermanagement.interface",
                                  QDBusConnection::systemBus());
    QDBusReply<bool> replay = powerInterface.call("CanSetBrightness");

    if(replay.isValid()) {
        m_powerManagerSetable = replay.value();
        if (m_powerManagerSetable) {
            m_brightnessManager = new PowerManagerBrightness(this);
        }
    }

    if (!m_powerManagerSetable) {
        if (UsdBaseClass::isWaylandWithKscreen()) {
            return;
        } else {
            m_gammaManagerSetable = true;
            m_brightnessManager = new GammaBrightness(this);
        }
    }

    if (m_brightnessManager != nullptr) {
        m_brightnessManager->connectTheSignal();
    }
}

uint Brightness::getPrimaryBrightness()
{
    if (!isEnable()) {
        return -1;
    }

    return m_brightnessManager->getBrightness();
}

bool Brightness::setPrimaryBrightness(uint Brightness)
{
    if (Brightness > 100) {
        return false;
    }

    if (!isEnable()) {
        return false;
    }

    m_brightnessManager->setBrightness(Brightness);
    return true;
}

uint Brightness::isEnable()
{
    return m_gammaManagerSetable | m_powerManagerSetable;
}

QString Brightness::backend()
{
    if (!isEnable()) {
        return "";
    }

    return m_brightnessManager->backend();
}


