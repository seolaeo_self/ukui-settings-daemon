#include "powermanager-brightness.h"


PowerManagerBrightness::PowerManagerBrightness(QObject *parent)
{
    m_pPowerManger = new QGSettings(POWER_MANAGER_SCHEMA);
}

int PowerManagerBrightness::getBrightness()
{
    return m_pPowerManger->get(BRIGHTNESS_AC_KEY).toInt();
}

int PowerManagerBrightness::setBrightness(int brightness)
{
    return m_pPowerManger->trySet(BRIGHTNESS_AC_KEY, brightness);
}

int PowerManagerBrightness::connectTheSignal()
{
    connect(m_pPowerManger, SIGNAL(changed(QString)), this, SLOT(doChanged(QString)));
}

QString PowerManagerBrightness::backend()
{
    return "power manager";
}

void PowerManagerBrightness::doChanged(QString key)
{
    if (key == BRIGHTNESS_AC_KEY) {
        int brightness = m_pPowerManger->get(key).toInt();
        if (brightness > 0 && brightness <= 100) {

            QDBusMessage notifySignal =
                    QDBusMessage::createSignal(DBUS_GC_BRIGHTNESS_PATH, DBUS_GC_BRIGHTNESS_INTERFACE, DBUS_GC_BRIGHTNESS_SIGNAL_PRIMARYCHANGED);

            notifySignal.setArguments({QVariant::fromValue(brightness)});
            QDBusConnection::sessionBus().send(notifySignal);
        }
    }
}
