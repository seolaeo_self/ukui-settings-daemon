#include "global-signal.h"

#include <QDebug>

GlobalSignal::GlobalSignal(QObject *parent)
{

    qRegisterMetaType<SessionStruct>("SessionStruct");
    qRegisterMetaType<SessionStructList>("SessionStructList");
    qDBusRegisterMetaType<SessionStruct>();
    qDBusRegisterMetaType<SessionStructList>();
    connectUserLogin1Signal();
//    connectUserActiveSignal1();
}

void GlobalSignal::sendUserActiveSignal(QString interface, QVariantMap qvarMap, QStringList strList)
{
    if (interface == "org.freedesktop.login1.Session") {
        QMap<QString,QVariant> mapKey = qvariant_cast<QMap<QString,QVariant>>(qvarMap);
        bool isActive = qvariant_cast<bool>(mapKey["Active"]);
        if (isActive) {
            QDBusMessage notifySignal =
                    QDBusMessage::createSignal(DBUS_GC_SIGNAL_PATH, DBUS_GC_SIGNAL_INTERFACE, DBUS_GC_SIGNAL_SIGNAL_Active);

            notifySignal.setArguments({QVariant::fromValue(isActive)});
            QDBusConnection::sessionBus().send(notifySignal);
            USD_LOG(LOG_DEBUG,"send active:%d",isActive);
        }
    }
}

void GlobalSignal::connectUserActiveSignal()
{
    m_plogin1 = new  DBusLogin1Interface("org.freedesktop.login1",
                              "/org/freedesktop/login1/user/self",
                              "org.freedesktop.login1.User",
                              QDBusConnection::systemBus());
    SessionStructList lqvar =  qvariant_cast<SessionStructList>(m_plogin1->property("Sessions"));

}

void GlobalSignal::connectUserActiveSignalWithPath(QString path)
{
    USD_LOG(LOG_DEBUG,"connect:%s..",path.toLatin1().data());
    m_ploginWithPath = new  QDBusInterface("org.freedesktop.login1",
                                           path,
                                           "org.freedesktop.DBus.Properties",
                                           QDBusConnection::systemBus());
   connect(m_ploginWithPath, SIGNAL(PropertiesChanged(QString,QVariantMap,QStringList)), this, SLOT(sendUserActiveSignal(QString,QVariantMap,QStringList)));
}

void GlobalSignal::connectUserLogin1Signal()
{
    m_plogin2 = new  QDBusInterface("org.freedesktop.login1",
                                    "/org/freedesktop/login1/user/self",
                                    "org.freedesktop.DBus.Properties",
                                    QDBusConnection::systemBus());


    QDBusMessage message = QDBusMessage::createMethodCall("org.freedesktop.login1",
                                                          "/org/freedesktop/login1/user/self"
                                                          ,"org.freedesktop.DBus.Properties",
                                                          "Get");

     QDBusMessage reply1 = m_plogin2->call("Get", "org.freedesktop.login1.User", "Sessions");

     QVariant v = reply1.arguments().first();
     QDBusArgument arg = v.value<QDBusVariant>().variant().value<QDBusArgument>();

     SessionStructList members;
     arg.beginArray();
     while(!arg.atEnd()) {
         SessionStruct m;
         arg >> m;
         members << m;
         USD_LOG(LOG_DEBUG,"ready connect %s..",m.userPath.path().toLatin1().data());
         connectUserActiveSignalWithPath(m.userPath.path());

         break;
     }
     arg.endArray();
}
