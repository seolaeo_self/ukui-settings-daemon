/* -*- Mode: C++; indent-tabs-mode: nil; tab-width: 4 -*-
 * -*- coding: utf-8 -*-
 *
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MEDIAKEYSETTINGS_H
#define MEDIAKEYSETTINGS_H

#include <QObject>
#include <QSharedPointer>
#include <QMap>
#include <QVariant>
#include <QGSettings/qgsettings.h>
#define UKUI_DAEMON_NAME "ukui-settings-daemon"

class MediaKeySettings : public QObject
{
    Q_OBJECT
    typedef QSharedPointer<QGSettings> GsettingsPtr;
    typedef QMap<QString , QVariant> GsettingsMap;

public:
    ~MediaKeySettings();
    static MediaKeySettings* instance();
    const QStringList getGsettingsKeys();
    QVariant getGsettingsValue(const QString& key);
    void initSettings();
    bool resultInitSettings();

    //恢复默认
    void resetSettings();
private:
    explicit MediaKeySettings(QObject *parent = nullptr);
    MediaKeySettings(const MediaKeySettings&) = delete;
    MediaKeySettings& operator =(const MediaKeySettings&)=delete;
private:
    void initMouseGsettings();
    void clearMapData();
private:
    GsettingsPtr m_settings;
    GsettingsMap m_keysData;

public Q_SLOTS:
    void onKeyChanged(const QString&);
Q_SIGNALS:
    void keyChanged(const QString&, QVariant);
};

#endif // MEDIAKEYSETTINGS_H
