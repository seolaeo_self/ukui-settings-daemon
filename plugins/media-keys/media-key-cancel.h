#ifndef MEDIAKEYCANCEL_H
#define MEDIAKEYCANCEL_H

#include <QObject>
#include <QDBusInterface>

class MediaKeyCancel : public QObject
{
    Q_OBJECT
public:
    explicit MediaKeyCancel(QObject *parent = nullptr);
    /**
     * @brief 注销当前组合所有kglobalaccel绑定
     * @param 组合
     */
    void unRegisterAll(const QString& component);
private:
    /**
     * @brief 获取所有actionid
     * @return
     */
    const QStringList getActionIds();
    /**
     * @brief 获取组合路径
     * @return
     */
    const QString getComponentPath();
    /**
     * @brief 注销actionId 绑定的键
     * @param actionId
     */
    void unRegisterShortcut(const QString& actionId);

private:
    QDBusInterface *m_kglobalaccel = nullptr;
    QString m_component;
};

#endif // MEDIAKEYCANCEL_H
