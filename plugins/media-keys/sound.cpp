/* -*- Mode: C++; indent-tabs-mode: nil; tab-width: 4 -*-
 * -*- coding: utf-8 -*-
 *
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "sound.h"
#include <QVariant>
extern "C" {
#include "clib-syslog.h"
}

#define SOUND_SCHEMA   "org.ukui.SettingsDaemon.plugins.media-keys-state"

#define UKUI_SOUND "org.ukui.sound"
#define MAX_VOLUME_NORMAL 100

static QString s_sinkVolume = "sink-volume";

static QString s_sinkMute = "sink-mute";

static QString s_sinkVolumeIncrease = "volume-increase-value";

Q_GLOBAL_STATIC(Sound, s_sound)
Sound::Sound(QObject *parent) : QObject(parent)
{
    if (!m_pulseAudio) {
        m_pulseAudio = new PulseAudioManager();
        connect(m_pulseAudio, SIGNAL(sinkVolumeChanged(int)), this, SLOT(doSinkVolumeChanged(int)));
        connect(m_pulseAudio, SIGNAL(sinkMuteChanged(bool)), this, SLOT(doSinkMuteChanged(bool)));
        m_pulseAudio->connectPulseContext();
    }

    if (QGSettings::isSchemaInstalled(QByteArray(SOUND_SCHEMA))) {
        m_soundSettings = new QGSettings(SOUND_SCHEMA);
        connect(m_soundSettings, SIGNAL(changed(QString)), this, SLOT(doSettingsChanged(const QString&)));
    }
}

Sound::~Sound()
{
    if (m_pulseAudio) {
        disconnect(m_pulseAudio, 0);
        m_pulseAudio->deleteLater();
        m_pulseAudio = nullptr;
    }
    if (m_soundSettings) {
        disconnect(m_soundSettings, SIGNAL(changed(QString)), this, SLOT(doSettingsChanged(const QString&)));
        m_soundSettings->deleteLater();
        m_soundSettings = nullptr;
    }
}

int Sound::getSinkVolume()
{
    return m_pulseAudio->getSinkVolume();
}

bool Sound::getSinkMute()
{
    return m_pulseAudio->getSinkMute();
}

bool Sound::getSourceMute()
{
    return m_pulseAudio->getSourceMute();
}

Sound* Sound::self()
{
    return s_sound;
}

void Sound::setSinkVolume(int volume)
{
    m_pulseAudio->setSinkVolume(volume);
}

void Sound::setSinkMute(bool mute)
{
    m_pulseAudio->setSinkMute(mute);
}

void Sound::setSourceMute(bool mute)
{
    m_pulseAudio->setSourceMute(mute);
}

void Sound::doSinkVolumeChanged(int value)
{
    if (m_soundSettings && m_soundSettings->keys().contains(s_sinkVolume)) {
        if (value != m_soundSettings->get(s_sinkVolume).toInt()) {
            m_soundSettings->set(s_sinkVolume, value);
            USD_LOG(LOG_DEBUG, "volume is %d", value);
        }
    } else {
        USD_LOG(LOG_DEBUG, "Sound settings is null or is not contains sink volume");
    }
}

void Sound::doSinkMuteChanged(bool value)
{
    if (m_soundSettings && m_soundSettings->keys().contains(s_sinkMute)) {
        if (value != m_soundSettings->get(s_sinkMute).toBool()) {
            m_soundSettings->set(s_sinkMute, value);
        }
    } else {
        USD_LOG(LOG_DEBUG, "Sound settings is null or is not contains sink mute");
    }
}

void Sound::doSettingsChanged(const QString &key)
{
    if (key == s_sinkVolume) {
        setSinkVolume(m_soundSettings->get(s_sinkVolume).toInt());
    } else if (key == s_sinkMute){
        setSinkMute(m_soundSettings->get(s_sinkMute).toBool());
    }
}
