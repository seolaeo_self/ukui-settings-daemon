/* -*- Mode: C++; indent-tabs-mode: nil; tab-width: 4 -*-
 * -*- coding: utf-8 -*-
 *
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MEDIAACTIONSETTINGS_H
#define MEDIAACTIONSETTINGS_H

#include <QObject>
#include <QSharedPointer>
#include <QGSettings/qgsettings.h>
#include <QMap>
#include <QVariant>

class MediaActionSettings : public QObject
{
    Q_OBJECT

public:
    explicit MediaActionSettings(QObject *parent = nullptr);

    static MediaActionSettings* instance();
    void initSettings();

    QVariant getPowerKeyState() const;
    QVariant getBrightnessValue() const;
    bool getCanSetBrightness();

    QVariant getTouchpadState() const;
    bool getSessionState();
    void setBrightnessValue(uint value);
    void setTouchpadState(bool state);
    void setLocatePointer();
private:
    QGSettings* initSettings(const QByteArray& schema, const QString& key);
    QVariant getValue(const QString& key, const QVariant& value);
    QVariant getValue(const QString& key);
    void initCanSetBrightness();
private:
    QMap<QString, QVariant> m_settingsData;
    QGSettings *m_touchpadSettings = nullptr;
    QGSettings *m_mouseSettings = nullptr;
    QGSettings *m_powerSettings = nullptr;
    QGSettings *m_sessionSettings = nullptr;
    QGSettings *m_shotSettings = nullptr;
};

#endif // MEDIAACTIONSETTINGS_H
