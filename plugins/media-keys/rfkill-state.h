/* -*- Mode: C++; indent-tabs-mode: nil; tab-width: 4 -*-
 * -*- coding: utf-8 -*-
 *
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RFKILLSTATE_H
#define RFKILLSTATE_H

#include <QObject>
#include "QGSettings/qgsettings.h"

class RfkillState : public QObject
{
    Q_OBJECT
public:
    explicit RfkillState(QObject *parent = nullptr);
    ~RfkillState();
    static RfkillState* self();
    void initialization();
    int getWlanState();
    void setWlanState(bool state);

    int getFlightState();
    void setFlightState(bool state);
    int getBluetooth();
    void setBluetooth(bool state);
    void setSettingsState(bool state);
Q_SIGNALS:
public Q_SLOTS:
    void doSettingsChangeAction(const QString& key);
private:
    QGSettings* m_rfkillSettings = nullptr;
};

#endif // RFKILLSTATE_H
