#include "rfkill-state.h"
#include "rfkillswitch.h"
#include <QDebug>

#define MEDIA_SCHEMA   "org.ukui.SettingsDaemon.plugins.media-keys-state"
static QString s_rfkillState = "rfkill-state";

Q_GLOBAL_STATIC(RfkillState, s_rfkill)

RfkillState::RfkillState(QObject *parent) : QObject(parent)
{

}

RfkillState::~RfkillState()
{
    if (m_rfkillSettings) {
        disconnect(m_rfkillSettings,SIGNAL(changed(QString)),  this, SLOT(doSettingsChangeAction(const QString&)));
        m_rfkillSettings->deleteLater();
        m_rfkillSettings = nullptr;
    }
}

RfkillState *RfkillState::self()
{
    return s_rfkill;
}

void RfkillState::initialization()
{
    if (QGSettings::isSchemaInstalled(QByteArray(MEDIA_SCHEMA))) {
        if (!m_rfkillSettings) {
            m_rfkillSettings = new QGSettings(MEDIA_SCHEMA);
        }
        connect(m_rfkillSettings, SIGNAL(changed(QString)),  this, SLOT(doSettingsChangeAction(const QString&)));
        if(m_rfkillSettings->keys().contains(s_rfkillState)) {
            int state = m_rfkillSettings->get(s_rfkillState).toInt();
            if(state >= 0) {
                if(-1 == getFlightState()) {
                    m_rfkillSettings->set(s_rfkillState, -1);
                } else {
                    setFlightState(state);
                }
            }
        }
    }
}

int RfkillState::getWlanState()
{
    return RfkillSwitch::instance()->getCurrentWlanMode();
}

void RfkillState::setWlanState(bool state)
{
    Q_UNUSED(state)
    RfkillSwitch::instance()->turnWifiOn();
}

int RfkillState::getFlightState()
{
    return RfkillSwitch::instance()->getCurrentFlightMode();
}

void RfkillState::setFlightState(bool state)
{
    RfkillSwitch::instance()->toggleFlightMode(state);
}

int RfkillState::getBluetooth()
{
    return RfkillSwitch::instance()->getCurrentBluetoothMode();
}

void RfkillState::setBluetooth(bool state)
{
    RfkillSwitch::instance()->toggleBluetoothMode(state);
}

void RfkillState::setSettingsState(bool state)
{
    if (m_rfkillSettings && m_rfkillSettings->keys().contains(s_rfkillState)) {
        m_rfkillSettings->set(s_rfkillState, state);
    }
}

void RfkillState::doSettingsChangeAction(const QString &key)
{
    if (key == s_rfkillState) {
        int state = m_rfkillSettings->get(s_rfkillState).toInt();
        if(state != -1) {
            setFlightState(state);
        }
    }
}
