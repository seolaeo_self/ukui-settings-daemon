/* -*- Mode: C++; indent-tabs-mode: nil; tab-width: 4 -*-
 * -*- coding: utf-8 -*-
 *
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "media-key-action.h"
#include "media-action-settings.h"
#include "rfkill-state.h"
#include "widget/pop-window-helper.h"
#include "sound.h"
#include "media-key-manager.h"
#include <QProcess>
#include <QVariant>
#include <QDBusConnection>
#include <QDBusInterface>
#include <QDBusReply>
#include <QDir>
#include <QTime>
#include <QDebug>
extern "C" {
#include "clib-syslog.h"
}

#define TIME_LIMIT(time) \
    static QTime startTime = QTime::currentTime();  \
    static int elapsed = -1;    \
    elapsed = startTime.msecsTo(QTime::currentTime());  \
    if(elapsed > 0 && elapsed <= time){  \
        /*USD_LOG(LOG_ERR,"time in TIME_LIMIT");*/    \
        return; \
    }   \
    startTime = QTime::currentTime();

Q_GLOBAL_STATIC(MediaKeyAction, s_mediaKeyAction)
MediaKeyAction::MediaKeyAction(QObject *parent) : QObject(parent)
{

}

MediaKeyAction *MediaKeyAction::self()
{
    return s_mediaKeyAction;
}

void MediaKeyAction::doAction(ActionType type)
{
    switch(type){
    case TOUCHPAD_KEY:
    case TOUCHPAD_ON_KEY:
    case TOUCHPAD_OFF_KEY:
        doTouchpadAction(type);
        break;
    case MUTE_KEY:
    case VOLUME_DOWN_KEY:
    case VOLUME_UP_KEY:
        doSoundAction(type);
        break;
    case MIC_MUTE_KEY:
        doMicrophonAction();
        break;
    case BRIGHT_UP_KEY:
    case BRIGHT_DOWN_KEY:
        doBrightnessAction(type);
        break;
    case POWER_DOWN_KEY:
        doSessionAction(POWER_SHUTDOWN);
        break;
    case POWER_OFF_KEY:
        doPowerKeyAction();
        break;
    case SHUTDOWN_MANAGEMENT_KEY:
        doSessionAction(POWER_INTER_ACTIVE);
        break;
    case EJECT_KEY:
        break;
    case HOME_KEY:
        doOpenHomeDirAction();
        break;
    case EMAIL_KEY:
        doOpenEvolutionAction();
        break;
    case SCREENSAVER_KEY:
        doScreensaverAction();
        break;
    case SETTINGS_KEY:
        doSettingsAction();
        break;
    case WINDOWSWITCH_KEY:
        doWindowSwitchAction();
        break;
    case FILE_MANAGER_KEY:
        doOpenFileManagerAction();
        break;
    case PLAY_KEY:
        doMultiMediaPlayerAction("Play");
        break;
    case PAUSE_KEY:
        doMultiMediaPlayerAction("Pause");
        break;
    case STOP_KEY:
        doMultiMediaPlayerAction("Stop");
        break;
    case PREVIOUS_KEY:
        doMultiMediaPlayerAction("Previous");
        break;
    case NEXT_KEY:
        doMultiMediaPlayerAction("Next");
        break;
    case REWIND_KEY:
        doMultiMediaPlayerAction("Rewind");
        break;
    case FORWARD_KEY:
        doMultiMediaPlayerAction("FastForward");
        break;
    case REPEAT_KEY:
        doMultiMediaPlayerAction("Repeat");
        break;
    case RANDOM_KEY:
        doMultiMediaPlayerAction("Shuffle");
        break;
    case TERMINAL_KEY:
        doOpenTerminalAction();
        break;
    case SCREENSHOT_KEY:
    case AREA_SCREENSHOT_KEY:
    case WINDOW_SCREENSHOT_KEY:
        doScreenshotAction(type);
        break;
    case SYSTEM_MONITOR_KEY:
        doOpenMonitor();
        break;
    case CONNECTION_EDITOR_KEY:
        doOpenNetworkEditor();
        break;
    case GLOBAL_SEARCH_KEY:
        doGlobalSearchAction();
        break;
    case KDS_KEY:
        doOpenKdsAction();
        break;
    case WLAN_KEY:
        doWlanAction();
        break;
    case WEBCAM_KEY:
        doWebcamAction();
        break;
    case UKUI_SIDEBAR:
        doSidebarAction();
        break;
    case UKUI_EYECARE_CENTER:
        doEyeCenterAction();
        break;
    case RFKILL_KEY:
        doFlightModeAction();
        break;
    case CALCULATOR_KEY:
        doOpenCalcAction();
        break;
    case BLUETOOTH_KEY:
        doBluetoothAction();
        break;
    case CURSOR_PROMPT_KEY:
        doLocatePointer();
        break;
    case ASRASSISTANT:
        doOpenAsrAssistant();
        break;
    default:
        break;
    }
}

void MediaKeyAction::doTouchpadAction(ActionType type)
{
    QVariant value = MediaActionSettings::instance()->getTouchpadState();
    if (value.isValid()) {
        bool state  = value.toBool();
        switch (type) {
        case TOUCHPAD_KEY:
            state = !state;
            break;
        case TOUCHPAD_ON_KEY:
            state = true;
            break;
        case TOUCHPAD_OFF_KEY:
            state = false;
            break;
        default:
            break;
        }
        MediaActionSettings::instance()->setTouchpadState(state);
        PopWindowHelper::self()->showWidget(state ? QStringLiteral("ukui-touchpad-on-symbolic") : QStringLiteral("ukui-touchpad-off-symbolic"));
    }
}

#define MIN_PROGRESSBAR 0
#define MAX_PROGRESSBAR 100
#define VOLUME_STEP 6

void MediaKeyAction::doSoundAction(ActionType type)
{
    int volumeStep = VOLUME_STEP;
    int volume  = Sound::self()->getSinkVolume();
    bool mute = Sound::self()->getSinkMute();
    int maxVolume = PopWindowHelper::self()->getMaxVolume();
    USD_LOG(LOG_DEBUG,"get sink volume  : %d" , volume);
    switch(type){
    case MUTE_KEY:
            mute = !mute;
        break;
    case VOLUME_DOWN_KEY:
        volume -= volumeStep;
        if(volume <= MIN_PROGRESSBAR){
            volume = MIN_PROGRESSBAR;
            mute = true;
        }else{
            mute = false;
        }
        break;
    case VOLUME_UP_KEY:
        if(mute){
            mute = false;
        }
        volume += volumeStep;
        if (volume >= maxVolume){
            volume = maxVolume;
        }
        break;
    default:break;
    }
    Sound::self()->setSinkVolume(volume);
    Sound::self()->setSinkMute(mute);
    PopWindowHelper::self()->showWidget(volume, mute);
}

void MediaKeyAction::doMicrophonAction()
{
    bool mute = Sound::self()->getSourceMute();
    Sound::self()->setSourceMute(!mute);
    PopWindowHelper::self()->showWidget(!mute ? "ukui-microphone-off-symbolic" : "ukui-microphone-on-symbolic");
}

#define STEP_BRIGHTNESS 5

void MediaKeyAction::doBrightnessAction(ActionType type)
{
    if (!MediaActionSettings::instance()->getCanSetBrightness()) {
        USD_LOG(LOG_WARNING, " This means that cant't set brightness by power ac ");
        return;
    }
    QVariant value = MediaActionSettings::instance()->getBrightnessValue();
    if (value.isValid()) {
        int brightness = value.toInt();
        switch (type) {
        case BRIGHT_UP_KEY:
            brightness += STEP_BRIGHTNESS;
            if (brightness >= MAX_PROGRESSBAR) {
                brightness = MAX_PROGRESSBAR;
            }
            break;
        case BRIGHT_DOWN_KEY:
            brightness -= STEP_BRIGHTNESS;
            if (brightness <= MIN_PROGRESSBAR) {
                brightness = MIN_PROGRESSBAR;
            }
            break;
        default:
            break;
        }
        MediaActionSettings::instance()->setBrightnessValue(brightness);
        PopWindowHelper::self()->showWidget(brightness);
    }
}

void MediaKeyAction::doSessionAction(PowerButton buttonType)
{
    QStringList arguments;
    switch (buttonType) {
    case POWER_HIBERNATE:
        arguments << "--hibernate";
        break;
    case POWER_INTER_ACTIVE:
        if (MediaActionSettings::instance()->getSessionState()) {
            USD_LOG(LOG_DEBUG, "session win-key-release is true");
            return;
        }
        break;
    case POWER_SHUTDOWN:
        arguments << "--shutdown";
        break;
    case POWER_SUSPEND:
        arguments << "--suspend";
        break;
    }
    executeCommand("ukui-session-tools", arguments);
}

void MediaKeyAction::doPowerKeyAction()
{
    //间隔 500 ms 内响应一次
    TIME_LIMIT(500);
    QVariant value = MediaActionSettings::instance()->getPowerKeyState();
    PowerButton buttonType = POWER_INTER_ACTIVE;
    if (value.isValid()) {
        buttonType = (PowerButton)value.toInt();
    } else {
        USD_LOG(LOG_WARNING, "get power key state is null");
    }
    doSessionAction(buttonType);
}

void MediaKeyAction::doOpenFileManagerAction(const QString& path)
{
    QStringList arguments;
    if (!path.isEmpty()) {
        arguments << "--show-folders" << path;
    }
    executeCommand("peony", arguments);
}

void MediaKeyAction::doOpenHomeDirAction()
{
    const QString& homePath = QDir::homePath();
    doOpenFileManagerAction(homePath);
}

void MediaKeyAction::doScreensaverAction()
{
    executeCommand("ukui-screensaver-command", "--lock");
}

void MediaKeyAction::doSettingsAction()
{
    executeCommand("/usr/bin/ukui-control-center");
}

void MediaKeyAction::doOpenCalcAction()
{
    executeCommand("kylin-calculator");
}

void MediaKeyAction::doOpenTerminalAction()
{
    executeCommand("x-terminal-emulator");
}

void MediaKeyAction::doOpenMonitor()
{
    executeCommand("ukui-system-monitor");
}

void MediaKeyAction::doOpenNetworkEditor()
{
    QDBusMessage message = QDBusMessage::createMethodCall("com.kylin.network",
                                                          "/com/kylin/network",
                                                          "com.kylin.network",
                                                          "showKylinNM");
    message << 2;
    QDBusMessage response = QDBusConnection::sessionBus().call(message);
    if(response.type() != QDBusMessage::ReplyMessage){
        executeCommand("kylin-nm", "--sw");
    }
}

void MediaKeyAction::doScreenshotAction(ActionType type)
{
    QStringList arguments;;
    switch (type) {
    case SCREENSHOT_KEY:
        arguments << "full";
        break;
    case AREA_SCREENSHOT_KEY:
        arguments << "gui";
        break;
    case WINDOW_SCREENSHOT_KEY:
        arguments << "screen";
        break;
    default:
        arguments << "gui";
        break;
    }
    executeCommand("kylin-screenshot", arguments);
}

void MediaKeyAction::doMultiMediaPlayerAction(const QString &operation)
{
    MediaKeyManager::instance()->doMultiMediaPlayerAction(operation);
}

void MediaKeyAction::doSidebarAction()
{
    QDBusMessage message = QDBusMessage::createMethodCall("org.ukui.Sidebar",
                                                          "/org/ukui/Sidebar",
                                                          "org.ukui.Sidebar",
                                                          "sidebarActive");
    QDBusMessage response = QDBusConnection::sessionBus().call(message);
    if (response.type() != QDBusMessage::ReplyMessage){
        executeCommand("ukui-sidebar", "-show");
    }
}

void MediaKeyAction::doWindowSwitchAction()
{
    executeCommand("ukui-window-switch", "--show-workspace");
}

void MediaKeyAction::doGlobalSearchAction()
{
    QDBusMessage message = QDBusMessage::createMethodCall("com.ukui.search.service",
                                                         "/",
                                                         "org.ukui.search.service",
                                                         "mainWindowSwitch");

    QDBusMessage response = QDBusConnection::sessionBus().call(message);
    if (response.type() != QDBusMessage::ReplyMessage){
        executeCommand("ukui-search", "-s");
    }
}

void MediaKeyAction::doOpenKdsAction()
{
    executeCommand("ukydisplayswitch");
}

void MediaKeyAction::doWlanAction()
{
    int wlanState = RfkillState::self()->getWlanState();
    if (wlanState == -1) {
        return;
    }
    QString icon;
    if (wlanState) {
        icon = QStringLiteral("network-wireless-connected-symbolic");
        RfkillState::self()->setWlanState(wlanState);
    } else {
        icon = QStringLiteral("network-wireless-offline-symbolic");
    }
    PopWindowHelper::self()->showWidget(icon);
}

void MediaKeyAction::doWebcamAction()
{
    QDBusInterface camera("com.settings.daemon.qt.systemdbus", \
                          "/camera", \
                          "com.settings.daemon.interface", \
                          QDBusConnection::systemBus());

    QDBusReply<int> reply2 = camera.call("getCameraEnable");

    if (reply2.isValid()) {
        int result = reply2.value();
        if (result < 0) {
            return;
        }
        camera.call("enableCamera", !result);
        PopWindowHelper::self()->showWidget(!result ? QStringLiteral("camera-switch-symbolic") : QStringLiteral("camera-switch-off-symbolic"));
    } else {
        USD_LOG(LOG_ERR,"camera dbus interface failed .");
    }
}

void MediaKeyAction::doEyeCenterAction()
{
    executeCommand("eye-protection-center");
}

void MediaKeyAction::doFlightModeAction()
{
    int flightState = RfkillState::self()->getFlightState();

    if(flightState == -1) {
        USD_LOG(LOG_ERR,"get flight mode error");
        return;
    }
    PopWindowHelper::self()->showWidget(flightState ? "ukui-airplane-mode-on-symbolic" : "ukui-airplane-mode-off-symbolic");
    RfkillState::self()->setSettingsState(flightState);
    //由于飞行模式热键不是usd 控制，所以这里获取改变后的状态发送信号，供侧边栏监听
//    Q_EMIT airModeStateChanged(flightState? true: false);
}

void MediaKeyAction::doBluetoothAction()
{

}

void MediaKeyAction::doOpenEvolutionAction()
{
    executeCommand("evolution");
}

void MediaKeyAction::doLocatePointer()
{
    MediaActionSettings::instance()->setLocatePointer();
}

void MediaKeyAction::doOpenAsrAssistant()
{
     executeCommand("kylin-asrassistant");
}

void MediaKeyAction::executeCommand(const QString& program, const QStringList &arguments)
{
    QProcess process;
    process.setProgram(program);
    process.setArguments(arguments);
    if (!process.startDetached()) {
        USD_LOG(LOG_DEBUG, "%s had a error", program.toLatin1().data());
    }
}

void MediaKeyAction::executeCommand(const QString &program, const QString &argument)
{
    executeCommand(program, QStringList()<<argument);
}
