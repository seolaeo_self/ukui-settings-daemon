/* -*- Mode: C++; indent-tabs-mode: nil; tab-width: 4 -*-
 * -*- coding: utf-8 -*-
 *
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MEDIAKEYMANAGER_H
#define MEDIAKEYMANAGER_H

#include <QObject>

#include "media-key-settings.h"
#include "media-key-binding.h"
#include "media-type.h"
#include "media-key-common.h"
extern "C" {
#include "usd_global_define.h"
}

typedef struct{
    QString application;
    uint time;
} MediaPlayer;

class MediaKeyManager : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface","org.ukui.SettingsDaemon.MediaKeys")
public:
    explicit MediaKeyManager(QObject *parent = nullptr);
    ~MediaKeyManager();
    static MediaKeyManager* instance();
    bool start();
    void stop();
public Q_SLOTS:
    void externalDoAction(int type, const QString& app);
    int getFlightState();
    void setFlightState(int value);

    /** two dbus method, will be called in mpris plugin(mprismanager.cpp MprisManagerStart())
     *  两个dbus 方法，将会在mpris插件中被调用(mprismanager.cpp MprisManagerStart())
     */
    void GrabMediaPlayerKeys(const QString& application);
    void ReleaseMediaPlayerKeys(const QString& application);

    void doMultiMediaPlayerAction(const QString& operation);
private:
    void registerDbusService();
    void doAction(ActionType type);

    bool findMediaPlayerByApplication(const QString &app);
    uint findMediaPlayerByTime(MediaPlayer* player);
    void removeMediaPlayerByApplication(const QString& app, uint currentTime);

private:
    /**
     * @brief 加载资源
     */
    void initResources();
    /**
     * @brief 声音
     */
    void initSound();
    /**
     * @brief rfkill
     */
    void initRfkill();
    void initXeventMonitor();

    void disconnectSettings();

    void clearShortcutList();

    void initShortcuts();
    void initStaticShortcuts();
    void initCustomShotrcuts();
    void connectSettings();
    /**
     * @brief 注册快捷键与现有冲突时，强行修改需要先注销冲突快捷键
     * @param shortcuts
     */
    void forcedConflictHandling(QString shortcuts);
private Q_SLOTS:
    void onKeysChanged(const QString& key, const QVariant& value);
    void MMhandleRecordEvent(uint);
    void MMhandleRecordEventRelease(uint);
Q_SIGNALS:
    void airModeStateChanged(bool state);
    void MediaPlayerKeyPressed(const QString& application, const QString& operation);
private:
    MediaKeySettings* m_mediaKeySettings = nullptr;

    QList<QSharedPointer<MediaKeyBinding>> m_customShortcuts;
    QList<QSharedPointer<MediaKeyBinding>> m_staticShortcuts;

    QList<MediaPlayer*> mediaPlayers;   //all opened media player(vlc,audacious) 已经打开的媒体播放器列表(vlc,audacious)

    xEventHandleHadRelase(MUTE_KEY);
    xEventHandleHadRelase(WLAN_KEY);
    xEventHandleHadRelase(MIC_MUTE_KEY);
    xEventHandleHadRelase(RFKILL_KEY);
    xEventHandleHadRelase(TOUCHPAD_KEY);
    xEventHandleHadRelase(TOUCHPAD_ON_KEY);
    xEventHandleHadRelase(TOUCHPAD_OFF_KEY);
    xEventHandleHadRelase(SCREENSAVER_KEY);
    xEventHandleHadRelase(WINDOWSWITCH_KEY);
    xEventHandleHadRelase(CALCULATOR_KEY);
    xEventHandleHadRelase(BLUETOOTH_KEY);
    xEventHandleHadRelase(WEBCAM_KEY);
    xEventHandleHadRelase(PLAY_KEY);
    xEventHandleHadRelase(STOP_KEY);
    xEventHandleHadRelase(PAUSE_KEY);
    xEventHandleHadRelase(RANDOM_KEY);
    xEventHandleHadRelase(REPEAT_KEY);
    xEventHandleHadRelase(SETTINGS_KEY);
    xEventHandleHadRelase(GLOBAL_SEARCH_KEY);
    xEventHandleHadRelase(MEDIA_KEY);
    xEventHandleHadRelase(EJECT_KEY);
    xEventHandleHadRelase(WWW_KEY);
    xEventHandleHadRelase(HELP_KEY);
    xEventHandleHadRelase(HOME_KEY);

};

#endif // MEDIAKEYMANAGER_H
