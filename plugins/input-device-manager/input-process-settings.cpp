/* -*- Mode: C++; indent-tabs-mode: nil; tab-width: 4 -*-
 * -*- coding: utf-8 -*-
 *
 * Copyright (C) 2022 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: sundagao <sundagao@kylinos.cn>
 */
#include "input-process-settings.h"
#include <QProcess>
#include <QDir>
extern "C" {
#include "clib-syslog.h"
}

ProcessSettings::ProcessSettings(QObject *parent) : QObject(parent)
{
    m_imwheelTimer = new QTimer(this);
    m_imwheelTimer->setSingleShot(false);
    m_imwheelTimer->start(12*1000);

    connect(m_imwheelTimer, &QTimer::timeout, this, [=](){
        m_rmeetingRunnig = CheckProcessAlive("Rmeet");
        if (m_rmeetingRunnig) {
            QString str="";
            if (m_rmeetingRunnig) {
                str = "killall imwheel";
                USD_LOG(LOG_DEBUG,"stop imwheel");
            } else {
                str = "/usr/bin/imwheel -k -b \"4 5\"";
                USD_LOG(LOG_DEBUG,"start imwheel");
            }
            QProcess::startDetached(str);
        }
    });
}

ProcessSettings *ProcessSettings::instance()
{
    static ProcessSettings instance;
    return &instance;
}

void ProcessSettings::setLocatePointer(bool state)
{
    if (state) {
        if (m_locatePointerRunning) {
            USD_LOG(LOG_DEBUG,"usd-locate-pointer is running .");
            return;
        }
        QString str = "usd-locate-pointer";
        m_locatePointerRunning = QProcess::startDetached(str);

    } else if (!state && m_locatePointerRunning) {
        QString str = "killall usd-locate-pointer";
        QProcess::startDetached(str);
        m_locatePointerRunning = false;
    }
}

void ProcessSettings::setMouseWheelSpeed(int speed)
{
    if (speed <= 0) {
        return;
    }
    QString FilePath = QDir::homePath() + "/.imwheelrc";
    QFile file;
    int delay = 2400 / speed;
    QString date = QString("\".*\"\n"
                   "Control_L, Up,   Control_L|Button4\n"
                   "Control_R, Up,   Control_R|Button4\n"
                   "Control_L, Down, Control_L|Button5\n"
                   "Control_R, Down, Control_R|Button5\n"
                   "Shift_L,   Up,   Shift_L|Button4\n"
                   "Shift_R,   Up,   Shift_R|Button4\n"
                   "Shift_L,   Down, Shift_L|Button5\n"
                   "Shift_R,   Down, Shift_R|Button5\n"
                   "None,      Up,   Button4, %1, 0, %2\n"
                   "None,      Down, Button5, %3, 0, %4\n"
                   "None,      Thumb1,  Alt_L|Left\n"
                   "None,      Thumb2,  Alt_L|Right")
                .arg(speed).arg(delay).arg(speed).arg(delay);

    file.setFileName(FilePath);

    if(file.open(QIODevice::WriteOnly | QIODevice::Text)){
        file.write(date.toLatin1().data());
    }
    file.close();

    if (m_rmeetingRunnig) {
        USD_LOG(LOG_DEBUG,"Rmeet is running .");
        return;
    }

    QString str = "imwheel -k -b \"4 5\"";
    QProcess::startDetached(str);
}


void ProcessSettings::setDisableWTypingSynaptics (bool state)
{
    if (state) {
        if (m_syndaemonRunning) {
            USD_LOG(LOG_DEBUG,"syndaemon is running .");
            return;
        }
        QString cmdStart = "syndaemon -i 0.3 -K -R";
        m_syndaemonRunning = QProcess::startDetached(cmdStart);

    } else if (!state && m_syndaemonRunning) {
        QString cmdKill = "killall syndaemon";
        QProcess::startDetached(cmdKill);
        m_syndaemonRunning = false;
    }
}
