/* -*- Mode: C++; indent-tabs-mode: nil; tab-width: 4 -*-
 * -*- coding: utf-8 -*-
 *
 * Copyright (C) 2022 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: sundagao <sundagao@kylinos.cn>
 */
#include "input-device.h"
#include "input-gsettings.h"

InputDevice::InputDevice(QVariant id, DeviceType type, QString name, QObject *parent)
    : m_deviceId(id)
    , m_type(type)
    , m_deviceName(name)
    , QObject(parent)
{

}

bool InputDevice::isMouse()
{
    return m_type == DeviceType::IN_MOUSE;
}

bool InputDevice::isTouchpad()
{
    return m_type == DeviceType::IN_TOUCHPAD;
}

QVariant InputDevice::getGsettingsValue(const QString &key)
{
    return InputGsettings::instance()->getGsettingsValue(key, m_type);
}
